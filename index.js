console.log ("Hello World!")

const getCube = [8**3]

message = `The cube of 8 is ${getCube}!`
console.log(message);

const address = {
	house_number: "258",
	street: "Washington Ave NW",
	state: "California",
	zip: "90011"
}

const {house_number, street, state, zip} = address
console.log(`I live at ${house_number} ${street} ${state} ${zip}`);

const animal = {
	type: "saltwater crocodile",
	weight: "1075 kgs",
	length: "20 ft 3 in"
}

const {type, weight, length} = animal
console.log(`Lolong was a ${type}. He weighed at ${weight} with a measurement of ${length}.`);

const numbers = [1, 2, 3, 4, 5,]

numbers.forEach((number) => {
	console.log(number);
})


let reduceNumber = numbers.reduce(
  (accumulator, currentValue) => accumulator + currentValue
);
console.log(reduceNumber);

const pet = {
	name: "Frankie",
	age: 5,
	breed: "Miniature Daschhund"
}

console.log(pet);
